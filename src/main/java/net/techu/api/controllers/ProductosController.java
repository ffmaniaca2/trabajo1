package net.techu.api.controllers;

import net.techu.api.Producto;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

@RestController
public class ProductosController {
    private static Map<String, Producto> listaProductos = new HashMap<>();

    /* Inicializar lista de productos */
    static {
        Producto pr1 = new Producto();
        pr1.setId("PR1");
        pr1.setNombre("PRODUCTO 1");
        listaProductos.put(pr1.getId(), pr1);

        Producto pr2 = new Producto();
        pr2.setId("PR2");
        pr2.setNombre("PRODUCTO 2");
        listaProductos.put(pr2.getId(), pr2);
    }


        @RequestMapping(value = "/productos/{id}", method = RequestMethod.GET)
    public ResponseEntity<Object> getProducto(@PathVariable("id") String id)
    {
        if (listaProductos.containsKey(id.toUpperCase()))
            return new ResponseEntity<>(listaProductos.get(id.toUpperCase()), HttpStatus.OK);
        return new ResponseEntity<>(String.format("No he encontrado el producto %s", id),
                                    HttpStatus.NOT_FOUND);
    }

    @PostMapping("/productos")
    public ResponseEntity<Object> addProducto(@RequestBody Producto productoNuevo)
    {
        if (listaProductos.containsKey(productoNuevo.getId().toUpperCase()))
            return new ResponseEntity<>("Producto ya existente", HttpStatus.CONFLICT);
        listaProductos.put(productoNuevo.getId(), productoNuevo);
        return new ResponseEntity<>(String.format("Producto %s dado de alta", productoNuevo.getId()),
                                    HttpStatus.CREATED);
    }

    @PutMapping("/productos/{id}")
    public ResponseEntity<Object> updateProducto(@PathVariable("id") String id,
                                                 @RequestBody Producto productoCambiado)
    {
        if (!id.toUpperCase().equals(productoCambiado.getId().toUpperCase()))
            return new ResponseEntity<>("Datos incorrectos", HttpStatus.CONFLICT);
        if (listaProductos.containsKey(id.toUpperCase())) {
            listaProductos.put(id, productoCambiado);
            return new ResponseEntity<>(String.format("Producto %s actualizado", id), HttpStatus.OK);
        }
        listaProductos.put(id, productoCambiado);
        return new ResponseEntity<>(String.format("Producto %s creado", id), HttpStatus.CREATED);
    }

    @DeleteMapping("/productos/{id}")
    public ResponseEntity<Object> deleteProducto(@PathVariable("id") String id)
    {
        if (listaProductos.containsKey(id.toUpperCase())) {
            listaProductos.remove(id);
            return new ResponseEntity<>(String.format("Producto %s eliminado", id), HttpStatus.OK);
        }
        return new ResponseEntity<>(String.format("Producto %s no encontrado", id), HttpStatus.NOT_FOUND);
    }

    @PatchMapping ("/productos/{id}")
    public ResponseEntity<Object> patchProducto(@PathVariable("id") String id,
                                                 @RequestBody Producto productoCambiado)
    {
        if (!id.toUpperCase().equals(productoCambiado.getId().toUpperCase()))
            return new ResponseEntity<>("Datos incorrectos", HttpStatus.CONFLICT);
        if (listaProductos.containsKey(id.toUpperCase())) {
            listaProductos.put(id, productoCambiado);
            return new ResponseEntity<>(String.format("Producto %s actualizado", id), HttpStatus.OK);
        }
        listaProductos.put(id, productoCambiado);
        return new ResponseEntity<>(String.format("Producto %s creado", id), HttpStatus.CREATED);
    }





    }
/*
    @RequestMapping("/productos")
    public String productos() {
        return "Lista de productos";
    }

    @RequestMapping("/productos")
    public String productos(@RequestParam(value = "id", required = true) String id) {
        return String.format("Info del producto %s", id);
    }
    */
