package net.techu.api;

public class Saludo {

    private String data;

    public Saludo(String msj)
    {
        this.data = msj;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}