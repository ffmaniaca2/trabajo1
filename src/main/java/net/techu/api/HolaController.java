package net.techu.api;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HolaController {

    @RequestMapping("/")
    public Saludo index() {
        Saludo saludo = new Saludo("Hola desde Spring Boot");
        return saludo;
    }
}